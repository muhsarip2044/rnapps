import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  Platform,
  StatusBar,
  TouchableOpacity   ,
  Image 
} from 'react-native';
import { Header, Left, Right, Center, Icon } from 'native-base' 
const { height, width } = Dimensions.get('window')

class HomeScreen extends Component {
	componentWillMount(){
		if (Platform.OS == 'android') {
	      this.startHeaderHeight = 50    +  StatusBar.currentHeight
	    }
	}
	render() {
		return (
			<View style={{ flex:1,paddingHorizontal: 20    }} >
				<Header style={{ backgroundColor: 'white'  ,paddingTop: StatusBar.currentHeight + 15 ,height:  StatusBar.currentHeight + 35      }}>
					<Left style={{  flex:1   }}>
						<TouchableOpacity>
							<Icon name="menu" onPress={ () => this.props.navigation.openDrawer()  } style={{ fontSize: 40   }}/> 
						</TouchableOpacity>
					</Left> 
				</Header>
				<View style={{flex:1, paddingHorizontal: 10     ,paddingTop: 20     }}>
					<View style={{ flex:3, flexDirection:'row' }}>
						<View style={styles.itemWidget}>
							<TouchableOpacity>
								<Image
							        style={{ height: 120, width:120   }}
							        source={require('../../../examples/rnavigation/assets/conversation.png')}
							      />    
							    <Text>User Managements   </Text>    
							</TouchableOpacity>
							 
						</View> 
						<View style={styles.itemWidget}>
							<TouchableOpacity>
								<Image
							        style={{ height: 120, width:120   }}
							        source={require('../../../examples/rnavigation/assets/newspaper.png')}
							      />    
							    <Text>User Management </Text>   
						    </TouchableOpacity>
						</View>      
					</View>
					<View style={{ flex:4 , flexDirection:'row' }}>
						<View style={styles.itemWidgetBottom}>
							<TouchableOpacity>
								<Image
							        style={{ height: 120, width:120   }}
							        source={require('../../../examples/rnavigation/assets/secure.png')}
							      />    
							    <Text>User Management </Text> 
							</TouchableOpacity>
							
						</View>
						<View style={styles.itemWidgetBottom}>
							<TouchableOpacity>
								<Image
							        style={{ height: 120, width:120   }}
							        source={require('../../../examples/rnavigation/assets/planet-earth.png')}
							      />     
							    <Text>User Management </Text> 
							</TouchableOpacity>  
							       
						</View>
					</View>
				</View> 
				
			</View>
		);
	}
}

const styles = StyleSheet.create({
	itemWidget:{
		paddingTop:20,   
		flex:1,
		alignItems:'center',
		justifyContent : 'center' , 
	},
	itemWidgetBottom:{
		flex:1,
		alignItems:'center', 
	}
});


export default HomeScreen;