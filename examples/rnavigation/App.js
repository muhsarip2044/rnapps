import React, { Component } from 'react'
import { createDrawerNavigator, DrawerItems  } from 'react-navigation'
import HomeScreen from '../../examples/rnavigation/screens/HomeScreen'
import SettingsScreen from '../../examples/rnavigation/screens/SettingsScreen' 

import { StyleSheet, View, Text, SafeAreaView, Dimensions , ScrollView, Image} from 'react-native';

const { height, width } = Dimensions.get('window')

class App extends Component {
  render() {
    return (
      <AppDrawerNavigator /> 
    );
  }
}

const CustomDrawerComponent = (props) => ( 
  <SafeAreaView>
    <View style={{height: 150, backgroundColor: '#fff',alignItems: 'center', justifyContent: 'center'    }}>
      <Image
        style={{ height: 120, width:120   }}
        source={require('../../examples/rnavigation/assets/admedikalogo.png')}
      />
      
    </View>
    <ScrollView>
      <DrawerItems  {...props} />
    </ScrollView>
  </SafeAreaView>
)

const AppDrawerNavigator = createDrawerNavigator({
  Home:HomeScreen,
  Settings:SettingsScreen
},{  
  contentComponent:CustomDrawerComponent,
  drawerWidth: width  ,
  contentOptions: {  
    activeTintColor:'red' 
  }
})

const styles = StyleSheet.create({

});


export default App; 