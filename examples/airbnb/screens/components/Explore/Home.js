import React from 'react';
import { StyleSheet, Text, View, Image,  } from 'react-native'
import StarRating from 'react-native-star-rating'

export default class Home extends React.Component {
  render() {
    return (
      <View style={{width:this.props.width/2-30,height:this.props.width/2-30, borderWidth:0.5, borderColor:'#dddddd'}}>
        <View style={{flex:1}}>
          <Image 
            source={ require('../../../../../examples/airbnb/assets/home.jpg') } 
            style={{ 
              flex:1,height:null,width:null,
              resizeMode:'cover' 
            }}
          />
        </View>
        <View style={{flex:1, alignItems:'flex-start',justifyContent:'space-evenly',paddingLeft:10  }}>
          <Text style={{ fontSize:14, color:'#b63838' }}>
            { this.props.type }
          </Text> 
          <Text style={{ fontSize:12, fontWeight:'bold' }}>
           { this.props.name }
          </Text>
          <Text style={{ fontSize:12  }}>
            { this.props.price }
          </Text>
          <StarRating 
            disable={true}
            maxStar={5}
            rating={this.props.rating}
            starSize={10}

          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
