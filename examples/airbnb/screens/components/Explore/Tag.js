import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Tag extends React.Component {
  render() {
    return (
      <View style={{minHeight:20, minWidth:40,padding:5,marginRight:5  , backgroundColor:'white',borderColor:'#dddddd',borderWidth:1 , borderRadius:2}}>
        <Text style={{fontSize:10, fontWeight:'700'}}>{ this.props.name }</Text>
      </View>
    );
  } 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
