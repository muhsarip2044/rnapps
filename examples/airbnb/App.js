import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { createBottomTabNavigator } from 'react-navigation' 
import Icon from 'react-native-vector-icons/Ionicons' 

import Explore from   '../../examples/airbnb/screens/Explore'
import Saved from '../../examples/airbnb/screens/Saved'
import Inbox from '../../examples/airbnb/screens/Inbox'
import Trips from '../../examples/airbnb/screens/Trips' 
import Profile from '../../examples/airbnb/screens/Profile'  



export default createBottomTabNavigator({ 
  Explore:{
    screen: Explore, 
    navigationOptions: {
      tabBarLabel: 'EXPLORE',
      tabBarIcon: ({ tintColor }) => {
        return <Icon name="ios-search-outline"  color={ tintColor } size={24} /> 
      } 
    } 
  }, 
  Saved: {
    screen: Saved,
    navigationOptions: {
      tabBarLabel: 'SAVED',
      tabBarIcon: ({ tintColor }) => {
        return <Icon name="ios-heart-outline"  color={ tintColor } size={24} /> 
      } 
    } 
  },
  Trips: {
    screen: Trips,
    navigationOptions: {
      tabBarLabel: 'TRIPS',
      tabBarIcon: ({ tintColor }) => {
        return <Image  source={ require('../../examples/airbnb/assets/airbnb.png') } style={{height:24,width:24, tintColor:tintColor}} /> 
      } 
    } 
  },
  Inbox: {
    screen: Inbox,
    navigationOptions: {
      tabBarLabel: 'INBOX',
      tabBarIcon: ({ tintColor }) => {
        return <Icon name="ios-chatboxes-outline"  color={ tintColor } size={24} /> 
      } 
    } 
  },
  Profile: {
    screen: Profile,
    navigationOptions: {
      tabBarLabel: 'PROFILE',
      tabBarIcon: ({ tintColor }) => {
        return <Icon name="ios-person-outline"  color={ tintColor } size={24} /> 
      }  
    }  
  }
},{
  tabBarOptions: {
    activeTintColor: 'red',
    inactiveTintColor:'grey',   
    style: {
      backgroundColor:'white',
      borderTopWidth:0,
      shadowOffset: { width:5, height:3 },
      shadowColor: 'black',
      shadowOpacity: 0.5 ,
      elevation:5  
    }
  }
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
